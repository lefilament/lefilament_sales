.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


===================
Le Filament - Sales
===================

This module depends upon *sale* module.

This module provides:
 - Updated quotation tree view to display untaxed amount instead of taxed amount
 - Updated sale order tree view to display untaxed amount instead of taxed amount
 - Updated sale order tree view to display amount that remains to be invoiced


Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>
* Remi Cazenave <remi@le-filament.com>
* Juliana Poudou <juliana@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
