# -*- coding: utf-8 -*-
{
    'name': 'Le Filament - Sales',

    'summary': "Generic Sales views update by Le Filament",

    'version': '10.0.1.0',
    'license': 'AGPL-3',
    'description': """
    Module Sales Le Filament

    This module depends upon *sale* module.

    This module provides updated tree views :
     - quotation to display untaxed amount instead of taxed amount
     - sale order to display untaxed amount instead of taxed amount
     - sale order to display amount that remains to be invoiced
    """,
    'author': 'LE FILAMENT',
    'category': 'Sales',
    'depends': ['sale'],
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
        'Rémi Cazenave <remi@le-filament.com>',
        'Juliana Poudou <juliana@le-filament.com>',
    ],
    'website': 'https://le-filament.com',
    'data': [
        'views/lefilament_sales_view.xml',
    ],
    'qweb': [
    ],
}
