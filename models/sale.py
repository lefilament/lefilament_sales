# -*- coding: utf-8 -*-

# © 2018 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class SaleOrder(models.Model):
    _name = "sale.order"
    _inherit = ['sale.order']
    _description = "Sales Order"
    _order = 'date_order desc, id desc'

    @api.depends('order_line.invoice_status')
    def _remain_to_invoice(self):
        """
        Compute the amount (untaxed) that remains to be invoiced
        from sale order
        """
        for order in self:
            if order.invoice_status == ('to invoice'):
                line_to_invoice = [line for line in order.order_line
                                   if line.invoice_status != 'no']

                untaxed_amount_to_invoice = order.amount_untaxed
                for line in line_to_invoice:
                    untaxed_amount_to_invoice -= (line.qty_invoiced
                                                  * line.price_unit)
            else:
                untaxed_amount_to_invoice = 0.0

            order.update({
                'untaxed_amount_to_invoice': untaxed_amount_to_invoice
            })

    untaxed_amount_to_invoice = fields.Monetary(
        string='Remains to invoice (untaxed)',
        compute='_remain_to_invoice',
        store=True,
        readonly=True,
        track_visibility='always')
